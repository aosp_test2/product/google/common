#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_PATH := $(call my-dir)

# Example Sensors HAL implementation.
include $(CLEAR_VARS)
LOCAL_MODULE := sensors.example
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MODULE_TAGS := optional
LOCAL_CFLAGS := -Wno-unused-parameter
LOCAL_SRC_FILES := \
  example_sensors.cpp \
  sensors_hal.cpp
include $(BUILD_SHARED_LIBRARY)

# Example app that uses sensors HAL.
include $(CLEAR_VARS)
LOCAL_MODULE := sensors-hal-example-app
LOCAL_SRC_FILES := hal-example-app.cpp
LOCAL_CFLAGS := -Wno-unused-parameter
LOCAL_SHARED_LIBRARIES := libhardware
include $(BUILD_EXECUTABLE)

# Example app that uses NDK sensors API.
include $(CLEAR_VARS)
LOCAL_MODULE := sensors-ndk-example-app
LOCAL_SRC_FILES := ndk-example-app.cpp
LOCAL_CFLAGS := -Wno-unused-parameter
LOCAL_SHARED_LIBRARIES := libsensor
include $(BUILD_EXECUTABLE)
